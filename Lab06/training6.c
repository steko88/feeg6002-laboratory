/* Laboratory 6, SESG6025, 2013/2014, Template */

#include <stdio.h>

/* Function void rstrip(char s[])
modifies the string s: if at the end of the string s there are one or more spaces,
then remove these from the string.

The name rstrip stands for Right STRIP, trying to indicate that spaces at the 'right'
end of the string should be removed.
*/
long string_length(char s[]) {
	long i=0;	 
	while(s[i]!='\0') {
	i++;
	}
return i;
}

void rstrip(char s[]) {
	long i=string_length(s);
	while (s[i]==32 || s[i]==0) {
		s[i]=0;
		i--;
	}
}


int main(void) {
  char test1[] = "Hello World   ";

  printf("Original string reads  : |%s|\n", test1);
  rstrip(test1);
  printf("r-stripped string reads: |%s|\n", test1);

  return 0;
}

