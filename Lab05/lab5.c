#include <stdio.h>
#define MAXLINE 1000 /* maximum length of string */

/* function prototype */
long string_length(char s[]);
void reverse(char source[], char target[]);

int main(void) {
  /*char original[] = "This is a test: can you print me in reverse character order?";*/
  char original[] = "Test";
  char reversed[MAXLINE];

  printf("%s\n", original);
  reverse(original, reversed);
  printf("%s\n", reversed);
  return 0;
}

long string_length(char s[]) {
	long i=0;	 
	while(s[i]!='\0') {
	i++;
	}
return i;
}

void reverse(char source[], char target[]) {
	int i=0;
	int l = string_length(source);
	if (l==0) {
		target[i]=0;
	}
	else {
	while(source[i]!='\0') {
		target[i]=source[l-(i+1)];
		i++;		
	}
	}
	target[l+1]=0;
}

