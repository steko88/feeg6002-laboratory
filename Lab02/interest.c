#include <stdio.h>

int main(void) {
	int s=1000;
	float debt = s;
	float rate = 0.03;
	int month;
	float interest, frac, total_interest;
	total_interest = 0;
	
	for(month=1;month<25;month++) {
		interest = debt * rate;
		debt = debt + interest;
		total_interest=total_interest+interest;
		frac = 100*total_interest/s;
		printf("month %2d: debt=%7.2f, interest=%5.2f, total_interest=%7.2f, frac=%6.2f%%\n",month,debt,interest,total_interest,frac);
	}

return 0;
}

